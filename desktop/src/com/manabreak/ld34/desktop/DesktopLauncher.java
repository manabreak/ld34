package com.manabreak.ld34.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings;
import com.manabreak.ld34.LDGame;

public class DesktopLauncher
{

    public static void main(String[] arg)
    {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        
        config.width = 480;
		config.height = 800;
		
        
		Settings s = new Settings();
		s.filterMag = TextureFilter.Nearest;
		s.filterMin = TextureFilter.Nearest;
		s.maxWidth = 1024;
		s.maxHeight = 1024;
		s.paddingX = 3;
		s.paddingY = 3;
		s.edgePadding = true;
		s.alias = false;
		s.bleed = true;
        s.duplicatePadding = true;
		s.useIndexes = false;
		
		TexturePacker.process(s, "../../images", "../../android/assets/graphics", "game");
        
        new LwjglApplication(new LDGame(), config);
    }
}
