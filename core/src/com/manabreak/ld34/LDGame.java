package com.manabreak.ld34;

import com.badlogic.gdx.Game;

public class LDGame extends Game
{

    @Override
    public void create()
    {
        Res.load();
        
        GameScreen screen = new GameScreen(this);
        setScreen(screen);
    }
}
