/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manabreak.ld34;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 *
 * @author Harri
 */
class Res
{
    private static TextureAtlas mAtlas;
    private static BitmapFont mFont;
    
    private static Music mIntro, mLoop;
    
    public static Sound mTire, mCrash;
    
    
    public static void playTireSound()
    {
       mTire.setLooping(0, true);
       mTire.resume();
    }
    
    public static void playCrash()
    {
        mCrash.play(0.5f, 1f, 0f);
    }
    
    public static void stopTireSound()
    {
        mTire.pause();
    }
    
    public static BitmapFont getFont()
    {
        return mFont;
    }
    
    public static Sprite createSprite(String name)
    {
        return mAtlas.createSprite(name);
    }
    
    public static TextureRegion getRegion(String name)
    {
        return mAtlas.findRegion(name);
    }
    
    public static void load()
    {
        mAtlas = new TextureAtlas(Gdx.files.internal("graphics/game.atlas"));
        mFont = new BitmapFont(Gdx.files.internal("font48.fnt"));
        
        mTire = Gdx.audio.newSound(Gdx.files.internal("sfx/tire.ogg"));
        mTire.setLooping(0, true);
        mTire.play(0.2f, 2f, 0f);
        mTire.pause();
        
        mCrash = Gdx.audio.newSound(Gdx.files.internal("sfx/crash.ogg"));
        
        mIntro = Gdx.audio.newMusic(Gdx.files.internal("sfx/intro.ogg"));
        mLoop = Gdx.audio.newMusic(Gdx.files.internal("sfx/loop.ogg"));
        mLoop.setLooping(true);
        
        mIntro.setOnCompletionListener(new Music.OnCompletionListener()
        {

            @Override
            public void onCompletion(Music music)
            {
                mLoop.play();
            }
        });
        
        mIntro.play();
    }
    
    public static void unload()
    {
        mAtlas.dispose();
        mFont.dispose();
    }
}
