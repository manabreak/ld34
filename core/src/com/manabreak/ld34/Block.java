/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manabreak.ld34;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import java.util.ArrayList;

/**
 *
 * @author Harri
 */
public class Block
{
    private final GameStage mStage;
    private ModelInstance mModel;
    private Body mBody;
    private BlockSpawner mSpawner;
    
    private BodyDef mBodyDef;
    /*
    private PolygonShape mShapeRight, mShapeLeft;
    private FixtureDef mFixtureDefLeft, mFixtureDefRight;
    */
    
    private ArrayList<PolygonShape> mShapes = new ArrayList<PolygonShape>();
    private ArrayList<FixtureDef> mFixtureDefs = new ArrayList<FixtureDef>();
    
    private int[] mLeft = null, mFront = null, mRight = null;
    
    public Block(BlockSpawner spawner, GameStage stage, Model model, int type)
    {
        mSpawner = spawner;
        mStage = stage;
        mModel = new ModelInstance(model);
        
        mBodyDef = new BodyDef();
        mBodyDef.type = BodyDef.BodyType.KinematicBody;
        
        final float B = 48f * C.SCR2BOX;
        
        switch(type)
        {
            case 0:
                {
                    PolygonShape shape = new PolygonShape();
                    shape.setAsBox(B, 3f * B, new Vector2(2f * B, 0f), 0f);
                    mShapes.add(shape);
                    shape = new PolygonShape();
                    shape.setAsBox(B, 3f * B, new Vector2(-2f * B, 0f), 0f);
                    mShapes.add(shape);
                    mFront = new int[] {0, 2, 4, 5, 6, 7, 8};
                    
                    // mFront = new int[] {8};
                }
                break;
            case 1:
                {
                    PolygonShape shape = new PolygonShape();
                    shape.setAsBox(B, B * 3f, new Vector2(-2f * B, 0f), 0f);
                    mShapes.add(shape);
                    shape = new PolygonShape();
                    shape.setAsBox(B * 3f, B, new Vector2(0f, -2f * B), 0f);
                    mShapes.add(shape);
                    shape = new PolygonShape();
                    shape.setAsBox(B, B, new Vector2(2f * B, 2f * B), 0f);
                    mShapes.add(shape);
                    mFront = new int[] {0, 2, 4, 5};
                }
                break;
            case 2:
                {
                    PolygonShape shape = new PolygonShape();
                    shape.setAsBox(B, B * 3f, new Vector2(-2f * B, 0f), 0f);
                    mShapes.add(shape);
                    shape = new PolygonShape();
                    shape.setAsBox(3f * B, B, new Vector2(0f, 2f * B), 0f);
                    mShapes.add(shape);
                    shape = new PolygonShape();
                    shape.setAsBox(B, B, new Vector2(2f * B, -2f * B), 0f);
                    mShapes.add(shape);
                    mRight = new int[] {3};
                }
                break;
            case 3:
                {
                    PolygonShape shape = new PolygonShape();
                    shape.setAsBox(B, B, new Vector2(-2f * B, 2f * B), 0f);
                    mShapes.add(shape);
                    shape = new PolygonShape();
                    shape.setAsBox(3f * B, B, new Vector2(0f, -2f * B), 0f);
                    mShapes.add(shape);
                    shape = new PolygonShape();
                    shape.setAsBox(B, 3f * B, new Vector2(2f * B, 0f), 0f);
                    mShapes.add(shape);
                    mFront = new int[] {0, 2, 4, 5};
                }
                break;
            case 4:
                {
                    PolygonShape shape = new PolygonShape();
                    shape.setAsBox(3f * B, B, new Vector2(0f, 2f * B), 0f);
                    mShapes.add(shape);
                    shape = new PolygonShape();
                    shape.setAsBox(B, 3f * B, new Vector2(2f * B, 0f), 0f);
                    mShapes.add(shape);
                    shape = new PolygonShape();
                    shape.setAsBox(B, B, new Vector2(-2f * B, -2f * B), 0f);
                    mShapes.add(shape);
                    mLeft = new int[] {1};
                }
                break;
            case 5:
                {
                    PolygonShape shape = new PolygonShape();
                    shape.setAsBox(B, B, new Vector2(0f, 0f), 0f);
                    mShapes.add(shape);
                    shape = new PolygonShape();
                    shape.setAsBox(B, 3f * B, new Vector2(-4f * B, 0f), 0f);
                    mShapes.add(shape);
                    shape = new PolygonShape();
                    shape.setAsBox(B, 3f * B, new Vector2(4f * B, 0f), 0f);
                    mShapes.add(shape);
                    mFront = new int[] {0, 2, 4};
                }
                break;
            case 6:
                {
                    PolygonShape shape = new PolygonShape();
                    shape.setAsBox(3f * B, B, new Vector2(0f, 0f), 0f);
                    mShapes.add(shape);
                    shape = new PolygonShape();
                    shape.setAsBox(B, 3f * B, new Vector2(-6f * B, 0f), 0f);
                    mShapes.add(shape);
                    shape = new PolygonShape();
                    shape.setAsBox(B, 3f * B, new Vector2(6f * B, 0f), 0f);
                    mShapes.add(shape);
                    shape = new PolygonShape();
                    shape.setAsBox(B, B, new Vector2(-4f * B, 4f * B), 0f);
                    mShapes.add(shape);
                    shape = new PolygonShape();
                    shape.setAsBox(B, B, new Vector2(4f * B, 4f * B), 0f);
                    mShapes.add(shape);
                    mFront = new int[] {0};
                }
                break;
            case 7:
                {
                    PolygonShape shape = new PolygonShape();
                    shape.setAsBox(B, B, new Vector2(-2f * B, 0f), 0f);
                    mShapes.add(shape);
                    shape = new PolygonShape();
                    shape.setAsBox(B, B, new Vector2(2f * B, 0f), 0f);
                    mShapes.add(shape);
                    shape = new PolygonShape();
                    shape.setAsBox(B, 3f * B, new Vector2(-6f * B, 0f), 0f);
                    mShapes.add(shape);
                    shape = new PolygonShape();
                    shape.setAsBox(B, 3f * B, new Vector2(6f * B, 0f), 0f);
                    mShapes.add(shape);
                    shape = new PolygonShape();
                    shape.setAsBox(B, B, new Vector2(-4f * B, 4f * B), 0f);
                    mShapes.add(shape);
                    shape = new PolygonShape();
                    shape.setAsBox(B, B, new Vector2(4f * B, 4f * B), 0f);
                    mShapes.add(shape);
                    mFront = new int[] {0};
                }
                break;
            case 8:
                {
                    PolygonShape shape = new PolygonShape();
                    shape.setAsBox(B, 3f * B, new Vector2(2f * B, 0f), 0f);
                    mShapes.add(shape);
                    shape = new PolygonShape();
                    shape.setAsBox(B, 3f * B, new Vector2(-2f * B, 0f), 0f);
                    mShapes.add(shape);
                    mFront = new int[] {0};
                    
                    // mFront = new int[] {7};
                }
                break;
        }
    }
    
    public int[] getLeft()
    {
        return mLeft;
    }
    
    public int[] getFront()
    {
        return mFront;
    }
    
    public int[] getRight()
    {
        return mRight;
    }
    
    void destroy()
    {
        if(mBody != null)
        {
            mStage.getWorld().destroyBody(mBody);
            mSpawner.remove(this);
            mBody = null;
        }
    }
    
    float mScale = 1f;
    
    public void spawn(float x, float y)
    {
        if(mBody == null)
        {
            mBody = mStage.getWorld().createBody(mBodyDef);
            for(PolygonShape s : mShapes)
            {
                mBody.createFixture(s, 1f);
            }
        }

        mScale = MathUtils.random(0.7f, 1.3f);
        
        setPosition(x, y, 96f);
    }
    
    public void setPosition(float x, float y, float z)
    {
        mBody.setTransform(x * C.SCR2BOX, y * C.SCR2BOX, mBody.getAngle());
        mModel.transform.setToScaling(1f, 1f, mScale).translate(x, y, z);
    }
    
    private Vector3 mTmp = new Vector3();
    
    public float getX()
    {
        return mModel.transform.getTranslation(mTmp).x;
    }
    
    public float getY()
    {
        return mModel.transform.getTranslation(mTmp).y;
    }
    
    public void act(float dt)
    {
        setPosition(getX(), getY() - 134f * dt * mStage.getCar().getSpeed() * mStage.getSpeedMultiplier(), 96f);
        
        // System.out.println(String.format("x: %.2f, y: %.2f", mBody.getPosition().x, mBody.getPosition().y));
        
        if(getY() < -500f)
        {
            System.out.println("Destroying building");
            destroy();
        }
    }

    ModelInstance getInstance()
    {
        return mModel;
    }

    private float mFrontY = 288f;
    
    float getFrontY()
    {
        return getY() + mFrontY;
    }
    
    float getLeftX()
    {
        return getX() - 288f;
    }
    
    float getRightX()
    {
        return getX() + 288f;
    }

    void remove()
    {
        if(mBody != null)
        {
            mStage.getWorld().destroyBody(mBody);
            mBody = null;
        }
    }
}
