/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manabreak.ld34;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import java.util.ArrayList;

/**
 *
 * @author Harri
 */
public class SmokeSpawner
{
    private class Smoke extends Entity
    {
        private float mTimer = 0f;
        private float mLife = 1f;
        
        private float mFrameTimer = 0f;
        private int mFrame = 0;
        
        public Smoke(GameStage stage, Sprite sprite)
        {
            super(stage, sprite);
        }
        
        public void spawn(float x, float y)
        {
            setOriginCenter();
            
            setRotation(MathUtils.random(0f, 360f));
            setScale(1f + MathUtils.random(-0.2f, 0.3f));
            setPosition(x - getWidth() / 2f, y - getHeight() / 2f);
            setColor(1f, 0.8f, 1f, 1f);
            mTimer = 0f;
            mStage.addActor(this);
            mSprite.setRegion(mRegions[0]);
            mFrame = 0;
            mFrameTimer = 0f;
            clearActions();
            setAlpha(1f);
        }

        @Override
        public void act(float delta)
        {
            super.act(delta);
            
            if(mFrame < 3)
            {
                mFrameTimer += delta;
                if(mFrameTimer >= 0.1f)
                {
                    mFrameTimer -= 0.1f;
                    mFrame++;
                    mSprite.setRegion(mRegions[mFrame]);
                    if(mFrame == 3)
                    {
                        addAction(Actions.fadeOut(0.5f, Interpolation.fade));
                    }
                }
            }
            
            moveBy(0f, -134f * delta * mStage.getSpeedMultiplier() * mStage.getCar().getSpeed());
            
            mTimer += delta;
            if(mTimer >= mLife)
            {
                remove();
            }
        }
    }
    
    private final GameStage mStage;
    private ArrayList<Smoke> mSmokes = new ArrayList<Smoke>();
    private int mIndex = 0;
    
    private TextureRegion[] mRegions = new TextureRegion[4];
    
    private float mSpawnTimer = 0f;
    private float mSpawnFreq = 0.06f;
    
    private Vector2 mSpawnPosTmp = new Vector2();
    
    public SmokeSpawner(GameStage stage)
    {
        mStage = stage;
        for(int i = 0; i < 4; ++i)
        {
            mRegions[i] = Res.getRegion("smoke" + i);
        }
        
        for(int i = 0; i < 40; ++i)
        {
            Smoke s = new Smoke(stage, Res.createSprite("smoke0"));
            mSmokes.add(s);
        }
    }
    
    private void spawnAt(float x, float y)
    {
        Smoke s = mSmokes.get(mIndex);
        mIndex++;
        mIndex %= mSmokes.size();
        s.spawn(x, y);
    }
    
    public void act(float dt)
    {
        mSpawnTimer += dt;
        if(mSpawnTimer >= mSpawnFreq)
        {
            mSpawnTimer -= mSpawnFreq;
            mSpawnPosTmp.set(3f, -1f);
            mSpawnPosTmp = mStage.getCar().localToStageCoordinates(mSpawnPosTmp);
            spawnAt(mSpawnPosTmp.x, mSpawnPosTmp.y);
            mSpawnPosTmp.set(15f, -1f);
            mSpawnPosTmp = mStage.getCar().localToStageCoordinates(mSpawnPosTmp);
            spawnAt(mSpawnPosTmp.x, mSpawnPosTmp.y);
        }
    }
}
