package com.manabreak.ld34;

import box2dLight.RayHandler;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import static com.badlogic.gdx.graphics.GL20.GL_COLOR_BUFFER_BIT;
import static com.badlogic.gdx.graphics.GL20.GL_DEPTH_BUFFER_BIT;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.PointLight;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

/**
 *
 * @author Harri
 */
class GameStage extends Stage implements ContactListener
{
    private final GameScreen mScreen;
    
    private World mWorld;
    private Box2DDebugRenderer mDebugRenderer;
    
    private Car mCar;
    
    private PerspectiveCamera mCamera;
    private OrthographicCamera mResultCam;
    private FrameBuffer mFB;
    
    private ModelBatch mModelBatch;
    private Environment mEnv;
    
    private PointLight mCarLight;
    
    private PointLight mBlueLight, mRedLight;
    
    private boolean mDebugDraw = false;
    private BlockSpawner mBlockSpawner;
    
    private float mSpeedMultiplier = 1f;
    
    private boolean mGameOver = false;
    
    private Entity mFade, mEndFlash;
    
    private SmokeSpawner mSmokeSpawner;
    private SkidSpawner mSkidSpawner;
    
    private Label mDistance, mLastDistance;
    private Label mRestartHint;
    
    private int mLastDist = 0;
    
    private RayHandler mRayHandler;
    
    box2dLight.PointLight mPL;
    
    public float getSpeedMultiplier()
    {
        return mSpeedMultiplier;
    }
    
    public GameStage(GameScreen screen)
    {
        super(new ExtendViewport(256, 256, new PerspectiveCamera()));
        mCamera = (PerspectiveCamera)getCamera();
        mResultCam = new OrthographicCamera();
        mScreen = screen;
        
        mFB = new FrameBuffer(Pixmap.Format.RGB888, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, true);
        mFB.getColorBufferTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
        
        mModelBatch = new ModelBatch();
        mEnv = new Environment();
        mEnv.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.3f, 0.01f, 0.1f, 1f));
        mCarLight = new PointLight();
        mCarLight.set(1f, 1f, 1f, 0f, 10f, 32f, 9000f);
        mEnv.add(mCarLight);
        
        mBlueLight = new PointLight();
        mBlueLight.set(0f, 0f, 1f, 100f, -getHeight() / 2f, 32f, 5000f);
        mEnv.add(mBlueLight);
        mRedLight = new PointLight();
        mRedLight.set(1f, 0f, 0f, -100f, -getHeight() / 2f, 32f, 5000f);
        mEnv.add(mRedLight);
        
        mCamera.fieldOfView = 110f;
        mCamera.position.set(0f, 50f, 170f);
        mCamera.lookAt(0f, 50f, 0f);
        mCamera.far = 300f;
        
        mWorld = new World(Vector2.Zero, true);
        mWorld.setContactListener(this);
        mDebugRenderer = new Box2DDebugRenderer();
        
        RayHandler.useDiffuseLight(true);
        mRayHandler = new RayHandler(mWorld);
        mRayHandler.setAmbientLight(0.2f, 0.2f, 0.2f, 0.5f);
        mPL = new box2dLight.PointLight(mRayHandler, 200, Color.WHITE, 50f, 0f, 0f);
        
        
        mCar = new Car(this);
        addActor(mCar);
        mPL.attachToBody(mCar.getBody());
        
        mBlockSpawner = new BlockSpawner(this);
        
        mFade = new Entity(this, Res.createSprite("white"));
        mEndFlash = new Entity(this, Res.createSprite("white"));
        // mEndFlash.setOriginCenter();
        
        mSmokeSpawner = new SmokeSpawner(this);
        mSkidSpawner = new SkidSpawner(this);
        
        LabelStyle lblStyle = new LabelStyle(Res.getFont(), Color.WHITE);
        mDistance = new Label("1234 m", lblStyle);
        // mDistance.setScale(0.25f);
        mDistance.setFontScale(0.5f);
        mDistance.setAlignment(Align.center);
        addActor(mDistance);
        
        mLastDistance = new Label("", lblStyle);
        mLastDistance.setFontScale(0.25f);
        mLastDistance.setAlignment(Align.center);
        
        mRestartHint = new Label("Press SPACE to restart", lblStyle);
        mRestartHint.setFontScale(0.35f);
        mRestartHint.setAlignment(Align.center);
        
        fadeIn();
    }

    public SkidSpawner getSkidSpawner()
    {
        return mSkidSpawner;
    }
    
    private void fadeIn()
    {
        mFade.setAlpha(1f);
        mFade.setSize(getWidth() * 1.5f, getHeight() * 1.5f);
        mFade.setColor(0f, 0f, 0f, 1f);
        mFade.setPosition(-getWidth() * 0.55f, -getHeight() * 0.55f);
        addActor(mFade);
        mFade.addAction(Actions.sequence(
            Actions.alpha(0f, 1f, Interpolation.fade),
            Actions.removeActor()
        ));
    }
    
    public void reset()
    {
        if(mCar.getTravelled() > mLastDist)
        {
            mLastDist = mCar.getTravelled();
        }
        
        addActor(mLastDistance);
        mLastDistance.setText("Best: " + mLastDist + " m");
        
        // mFade.remove();
        mRestartHint.remove();
        fadeIn();
        mEndFlash.remove();
        mCar.reset();
        mBlockSpawner.reset();
        mCamera.position.x = mCar.getMiddleX();
        mCamera.position.y = mCar.getMiddleY();
        mCamera.update();
        mSpeedMultiplier = 1f;
        mGameOver = false;
    }
    
    public Car getCar()
    {
        return mCar;
    }
    
    private float mLightTimer = 0f;
    private boolean mLightsFlip = true;
    
    @Override
    public void act(float delta)
    {
        if(mGameOver)
        {
            actGameOver(delta);
            return;
        }
        
        mSpeedMultiplier += delta / 60f;
        
        mWorld.step(delta, 6, 10);
        super.act(delta);
        mBlockSpawner.act(delta);
        mSmokeSpawner.act(delta);
        
        if(Gdx.input.isKeyJustPressed(Keys.F1))
        {
            mDebugDraw = !mDebugDraw;
        }
        
        mLightTimer += delta;
        if(mLightTimer >= 1f)
        {
            mLightTimer -= 1f;
            mLightsFlip = !mLightsFlip;
        }
        
        mCarLight.position.x = mCar.getX();
        if(mLightsFlip)
        {
            mRedLight.position.x = mCar.getMiddleX() + 100f;
            mBlueLight.position.x = mCar.getMiddleX() - 100f;
        }
        else
        {
            mRedLight.position.x = mCar.getMiddleX() - 100f;
            mBlueLight.position.x = mCar.getMiddleX() + 100f;
        }
        
        mCamera.position.x = mCamera.position.x + (mCar.getMiddleX() - mCamera.position.x) * delta * 10f;
        mCamera.position.y = mCamera.position.y + (mCar.getMiddleY() - mCamera.position.y) * delta * 10f;
        mCamera.update();
        
        
        mDistance.setText(String.format("%d m", mCar.getTravelled()));
        mDistance.setPosition(mCamera.position.x - mDistance.getWidth() / 2f, -200f);
        
        mLastDistance.setPosition(mCamera.position.x, mCamera.position.y - getHeight() / 2f);
    }

    @Override
    public void draw()
    {
        mFB.begin();
        Gdx.gl20.glClear(GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        mModelBatch.begin(mCamera);
        for(Block b : mBlockSpawner.getActiveBuildings())
        {
            mModelBatch.render(b.getInstance(), mEnv);
        }
        mModelBatch.end();
        
        
        
        mFB.end();
        
        Gdx.gl20.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        
        
        getBatch().setProjectionMatrix(mResultCam.combined);
        getBatch().begin();
        getBatch().draw(mFB.getColorBufferTexture(), -1f, 1f, 2f, -2f);
        getBatch().end();
        
        // mRayHandler.setCombinedMatrix(getCamera().combined, 0f, 0f, getWidth() * C.BOX2SCR, getHeight() * C.BOX2SCR);
        
        Matrix4 mat = getCamera().combined.cpy();
        mat.scl(C.BOX2SCR);
        mRayHandler.setCombinedMatrix(mat);
        mRayHandler.updateAndRender();
        
        super.draw();
        
        
        
        if(mDebugDraw)
        {
            Matrix4 m = getCamera().combined.cpy();
            m.scl(C.BOX2SCR);
            mDebugRenderer.render(mWorld, m);
        }
    }

    World getWorld()
    {
        return mWorld;
    }

    public BlockSpawner getBlockSpawner()
    {
        return mBlockSpawner;
    }

    @Override
    public void beginContact(Contact contact)
    {
        Body bodyA = contact.getFixtureA().getBody();
        Body bodyB = contact.getFixtureB().getBody();
        
        if(bodyA == getCar().getBody())
        {
            initGameOver();
        }
    }

    private float mGameOverCamX = 0f, mGameOverCamY = 0f;
    private boolean mCamShaking = false;
    
    private void initGameOver()
    {
        System.out.println("Game over!");
        mGameOver = true;
        
        mGameOverCamX = getCamera().position.x;
        mGameOverCamY = getCamera().position.y;
        mCamShaking = true;
        
        mFade.clearActions();
        mFade.setSize(getWidth() * 1.5f, getHeight() * 1.5f);
        mFade.setColor(0f, 0f, 0f, 0f);
        mFade.setPosition(mCar.getMiddleX() - mFade.getWidth() / 2f, mCar.getMiddleY() - mFade.getHeight() / 2f);
        addActor(mFade);
        mFade.addAction(Actions.sequence(
            Actions.alpha(1f, 0.2f, Interpolation.fade)
        ));
        
        mEndFlash.clearActions();
        mEndFlash.setColor(1f, 1f, 1f, 1f);
        mEndFlash.setSize(1f, 1f);
        mEndFlash.setOriginCenter();
        mEndFlash.setPosition(mCar.getMiddleX(), mCar.getMiddleY());
        addActor(mEndFlash);
        mEndFlash.addAction(Actions.parallel(
            Actions.sequence(
                Actions.sizeTo(getWidth() * 1.5f, 4f, 0.1f, Interpolation.linear),
                Actions.sizeTo(getWidth() * 1.5f, 0f, 1f, Interpolation.circleIn),
                Actions.run(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        mCamShaking = false;
                    }
                }),
                Actions.parallel(
                    Actions.sizeTo(0f, 0f, 3f, Interpolation.fade)
                )
                // Actions.sizeTo(getWidth() * 1.5f, getHeight() * 1.5f, 0.1f, Interpolation.linear)
            ),
            Actions.sequence(
                Actions.color(new Color(1f, 0f, 1f, 1f), 0.2f, Interpolation.linear),
                Actions.color(new Color(0f, 0f, 0f, 1f), 4f, Interpolation.fade)
            )
        ));
        
        mDistance.clearActions();
        addActor(mDistance);
        mDistance.addAction(
            Actions.moveTo(getCamera().position.x - mDistance.getWidth() / 2f, 0f, 0.6f, Interpolation.fade)
        );
        
        mRestartHint.clearActions();
        addActor(mRestartHint);
        mRestartHint.setColor(1f, 1f, 1f, 0f);
        mRestartHint.setPosition(getCamera().position.x - mRestartHint.getWidth() / 2f, -26f);
        mRestartHint.addAction(Actions.alpha(1f, 3f, Interpolation.fade));
        
        Res.playCrash();
        Res.stopTireSound();
    }
    
    @Override
    public void endContact(Contact contact)
    {
        
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold)
    {
        
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse)
    {
        
    }

    private void actGameOver(float delta)
    {
        if(mCamShaking)
        {
            float cx = mGameOverCamX + MathUtils.random(-5f, 5f);
            float cy = mGameOverCamY + MathUtils.random(-5f, 5f);
            getCamera().position.x = cx;
            getCamera().position.y = cy;
            getCamera().update();
        }
        
        super.act(delta);
        mEndFlash.setPosition(mCar.getMiddleX() - mEndFlash.getWidth() / 2f, mCar.getMiddleY() - mEndFlash.getHeight() / 2f);
        
        if(Gdx.input.isKeyJustPressed(Keys.SPACE))
        {
            reset();
        }
    }

    boolean isGameOver()
    {
        return mGameOver;
    }

    
}
