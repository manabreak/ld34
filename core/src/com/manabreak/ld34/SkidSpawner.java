/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manabreak.ld34;

import com.badlogic.gdx.graphics.g2d.Sprite;
import java.util.ArrayList;

/**
 *
 * @author Harri
 */
public class SkidSpawner
{
    
    private class Skid extends Entity
    {
        public Skid(GameStage stage, Sprite sprite)
        {
            super(stage, sprite);
        }
        
        void spawn(float x, float y, float r)
        {
            setPosition(x, y);
            setRotation(r);
            mStage.addActor(this);
        }

        @Override
        public void act(float delta)
        {
            super.act(delta);
            moveBy(0f, -134f * delta * mStage.getSpeedMultiplier() * mStage.getCar().getSpeed());
            
            if(getY() < -500f)
            {
                remove();
            }
        }
        
        
    }
    
    private GameStage mStage;
    private ArrayList<Skid> mPool = new ArrayList<Skid>();
    private int mIndex = 0;
    
    public SkidSpawner(GameStage stage)
    {
        mStage = stage;
        
        for(int i = 0; i < 50; ++i)
        {
            Skid s = new Skid(stage, Res.createSprite("skidmark"));
            s.setColor(0.4f, 0.4f, 0.4f, 0.5f);
            mPool.add(s);
        }
    }
    
    public void spawn(float x, float y, float r)
    {
        Skid s = mPool.get(mIndex++);
        mIndex %= mPool.size();
        
        s.spawn(x, y, r);
    }
}
