/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manabreak.ld34;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.scenes.scene2d.Group;

/**
 *
 * @author Harri
 */
public class Car extends Group
{
    private static final float STEER_FORCE = 15f;
    private static final float MAX_VEL_X = 10f;
    private static final float STEER_TILT_FORCE = 4f;
    private static final float MAX_STEER_ANGLE = 90f * MathUtils.degRad;

    private float mSpeed = 1f;
    
    private int mTravelled = 0;
    
    public float getSpeed()
    {
        return mSpeed;
    }

    Body getBody()
    {
        return mBody;
    }

    
    
    private interface CarInput
    {
        void act(float dt);
    }
    
    private class DesktopInput implements CarInput
    {
        private boolean handleInput(float dt)
        {
            boolean steering = false;
            if(Gdx.input.isKeyPressed(Keys.LEFT))
            {
                steering = true;
                if(mBody.getLinearVelocity().x > -MAX_VEL_X * mStage.getSpeedMultiplier())
                {
                    mBody.applyForceToCenter(-STEER_FORCE * mStage.getSpeedMultiplier(), 0f, true);
                }
            }
            if(Gdx.input.isKeyPressed(Keys.RIGHT))
            {
                steering = true;
                if(mBody.getLinearVelocity().x < MAX_VEL_X * mStage.getSpeedMultiplier())
                {
                    mBody.applyForceToCenter(STEER_FORCE * mStage.getSpeedMultiplier(), 0f, true);
                }
            }
            
            return steering;
        }
        
        @Override
        public void act(float dt)
        {
            if(mStage.isGameOver()) return;
            
            boolean steering = handleInput(dt);
            
            if(steering)
            {
                if(Gdx.input.isKeyPressed(Keys.RIGHT))
                {
                    float a = mBody.getAngle();
                    a = MathUtils.lerpAngle(a, -MAX_STEER_ANGLE, dt * 3f * mStage.getSpeedMultiplier());
                    mBody.setTransform(mBody.getPosition(), a);
                }
                if(Gdx.input.isKeyPressed(Keys.LEFT))
                {
                    float a = mBody.getAngle();
                    a = MathUtils.lerpAngle(a, MAX_STEER_ANGLE, dt * 3f * mStage.getSpeedMultiplier());
                    mBody.setTransform(mBody.getPosition(), a);
                }
                
            }
            else
            {
                Vector2 vel = mBody.getLinearVelocity();
                vel = vel.scl(-1f);
                mBody.applyForceToCenter(vel, true);
                
                float a = mBody.getAngle();
                a = MathUtils.lerpAngle(a, 0f, dt * 3f);
                mBody.setAngularVelocity(0f);
                mBody.setTransform(mBody.getPosition().x, mBody.getPosition().y, a);
            }
        }
        
    }
    
    private GameStage mStage;
    
    private Entity mCarShadow;
    private Entity mCarBody;
    
    private CarInput mInput;
    
    private Body mBody;
    private float mWidth, mHeight, mHalfWidth, mHalfHeight;
    
    public Car(GameStage stage)
    {
        mStage = stage;
        mCarShadow = new Entity(stage, Res.createSprite("shadow"));
        addActor(mCarShadow);
        mCarShadow.setPosition(-2f, -6f);
        mCarBody = new Entity(stage, Res.createSprite("car0"));
        addActor(mCarBody);
        
        mWidth = mCarBody.getWidth();
        mHeight = mCarBody.getHeight();
        mHalfWidth = mWidth / 2f;
        mHalfHeight = mHeight / 2f;
        
        setPosition(-mHalfWidth, -mHalfHeight);
        setOrigin(mHalfWidth, mHalfHeight);
        
        if(Gdx.app.getType() == Application.ApplicationType.Desktop)
        {
            mInput = new DesktopInput();
        }
        
        createPhysicsBody();
        
    }
    
    void reset()
    {
        setPosition(-mHalfWidth, -mHalfHeight);
        mBody.setTransform(0f, 0f, 0f);
        mBody.setAngularVelocity(0f);
        mBody.setLinearVelocity(Vector2.Zero);
        mTravelled = 0;
        setRotation(0f);
        
    }
    
    private void createPhysicsBody()
    {
        BodyDef def = new BodyDef();
        def.type = BodyDef.BodyType.DynamicBody;
        def.fixedRotation = false;
        mBody = mStage.getWorld().createBody(def);
        
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(mCarBody.getWidth() / 2f * C.SCR2BOX, mCarBody.getHeight() / 2f * C.SCR2BOX);
        
        FixtureDef fd = new FixtureDef();
        fd.density = 0.1f;
        fd.friction = 0.998f;
        fd.isSensor = false;
        fd.restitution = 0.01f;
        fd.shape = shape;
        
        Fixture f = mBody.createFixture(fd);
        
        shape.dispose();
    }
    
    int getTravelled()
    {
        return mTravelled;
    }
    
    @Override
    public void act(float delta)
    {
        // super.act(delta);
        if(mStage.isGameOver()) return;
        
        mInput.act(delta);
        
        float oldSpeed = mSpeed;
        mSpeed = MathUtils.cos(mBody.getAngle());
        
        setRotation(mBody.getAngle() * MathUtils.radDeg);
        setPosition(mBody.getPosition().x * C.BOX2SCR - mHalfWidth, mBody.getPosition().y * C.BOX2SCR - mHalfHeight);
        
        mTravelled += (MAX_VEL_X * mStage.getSpeedMultiplier()) / 10;
        
        if(mSpeed > 0.15f && mSpeed < 0.85f || (Math.abs(mSpeed) - Math.abs(oldSpeed) < -0.2f))
        {
            Vector2 v = localToStageCoordinates(new Vector2(1f, 0f));
            mStage.getSkidSpawner().spawn(v.x, v.y, getRotation());
            Res.playTireSound();
            
        }
        else
        {
            Res.stopTireSound();
        }
        
        // System.out.println("Travelled: " + mTravelled);
    }
    
    float getMiddleX()
    {
        return getX() + mHalfWidth;
    }
    
    float getMiddleY()
    {
        return getY() + mHalfHeight;
    }
    
}
