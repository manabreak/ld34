/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manabreak.ld34;

/**
 * Constants used throughout the game.
 *
 * @author Harri
 */
public class C
{
    public static final float BOX2SCR = 16f;
    public static final float SCR2BOX = 1f / BOX2SCR;
}
