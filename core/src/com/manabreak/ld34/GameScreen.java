package com.manabreak.ld34;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 *
 * @author Harri
 */
class GameScreen implements Screen
{
    private final LDGame mGame;
    private GameStage mGameStage;
    
    private Stage mIntroStage;
    private Stage mCurrentStage;
    
    GameScreen(LDGame game)
    {
        mGame = game;
        mIntroStage = new IntroStage(this);
        mGameStage = new GameStage(this);
        
        // mCurrentStage = mIntroStage;
        mCurrentStage = mGameStage;
    }

    @Override
    public void show()
    {
        
    }
    
    public void swapToGame()
    {
        mCurrentStage = mGameStage;
    }

    @Override
    public void render(float delta)
    {
        Gdx.gl20.glClearColor(0, 0, 0, 1);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
        mCurrentStage.act(delta);
        mCurrentStage.draw();
    }

    @Override
    public void resize(int width, int height)
    {
        
    }

    @Override
    public void pause()
    {
        
    }

    @Override
    public void resume()
    {
        
    }

    @Override
    public void hide()
    {
        
    }

    @Override
    public void dispose()
    {
        
    }
    
}
