/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manabreak.ld34;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

/**
 *
 * @author Harri
 */
public class IntroStage extends Stage
{
    private GameScreen mScreen;
    
    private Entity mLogoLeft, mLogoRight;
    
    public IntroStage(GameScreen screen)
    {
        super(new ExtendViewport(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2));
        mScreen = screen;
        
        mLogoLeft = new Entity(null, Res.createSprite("logo_left"));
        mLogoRight = new Entity(null, Res.createSprite("logo_right"));
        
        mLogoLeft.setPosition(0f, getHeight() / 2f);
        mLogoRight.setPosition(getWidth() / 2f, getHeight() / 2f + 5f);
        
        mLogoLeft.setAlpha(0f);
        mLogoRight.setAlpha(0f);
        
        mLogoLeft.addAction(Actions.parallel(
            Actions.moveBy(50f, 0f, 6f, Interpolation.linear),
            Actions.sequence(
                Actions.alpha(1f, 2f, Interpolation.fade),
                Actions.delay(2f),
                Actions.alpha(0f, 2f, Interpolation.fade)
            )
        ));
        
        mLogoRight.addAction(Actions.sequence(
            Actions.delay(2f),
            Actions.parallel(
                Actions.moveBy(50f, 0f, 6f, Interpolation.linear),
                Actions.sequence(
                    Actions.alpha(1f, 2f, Interpolation.fade),
                    Actions.delay(2f),
                    Actions.alpha(0f, 2f, Interpolation.fade)
                )
            ),
            Actions.run(new Runnable()
            {
                @Override
                public void run()
                {
                    mScreen.swapToGame();
                }
            })
        ));
        
        addActor(mLogoLeft);
        addActor(mLogoRight);
    }
}
