/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manabreak.ld34;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.loader.ObjLoader;
import com.badlogic.gdx.math.MathUtils;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Harri
 */
public class BlockSpawner
{
    private static final int BLOCK_TYPES = 9;
    
    private final GameStage mStage;
    private final ArrayList<ArrayList<Block>> mPool;
    private final int[] mIndices = new int[BLOCK_TYPES];
    private final ArrayList<Block> mActive = new ArrayList<Block>();
    private final ArrayList<Model> mModels = new ArrayList<Model>(BLOCK_TYPES);
    private final ArrayList<Block> mOpenRoads = new ArrayList<Block>(10);
    
    private Block mLast = null;
    
    public BlockSpawner(GameStage stage)
    {
        this.mPool = new ArrayList<ArrayList<Block>>();
        mStage = stage;
        
        ObjLoader loader = new ObjLoader();
        
        
        for(int i = 0; i < BLOCK_TYPES; ++i)
        {
            mIndices[i] = 0;
            
            Model model = loader.loadModel(Gdx.files.internal("b" + i + ".obj"), true);
            mModels.add(model);
            
            mPool.add(new ArrayList<Block>());
            for(int j = 0; j < 10; ++j)
            {
                Block b = new Block(this, stage, model, i);
                mPool.get(i).add(b);
            }
        }
        
        Block b = mPool.get(0).get(0);
        mIndices[0]++;
        b.spawn(0f, 0f);
        mActive.add(b);
        mOpenRoads.add(b);
        mLast = b;
    }
    
    void reset()
    {
        for(ArrayList<Block> p : mPool)
        {
            for(Block b : p)
            {
                b.remove();
            }
        }
        
        mActive.clear();
        mOpenRoads.clear();
        
        for(int i = 0; i < mIndices.length; ++i)
        {
            mIndices[i] = 0;
        }
        
        Block b = mPool.get(0).get(0);
        mIndices[0]++;
        b.spawn(0f, 0f);
        mActive.add(b);
        mOpenRoads.add(b);
        mLast = b;
    }
    
    public void act(float dt)
    {
        if(mRemoves.size() > 0)
        {
            mActive.removeAll(mRemoves);
            mOpenRoads.removeAll(mRemoves);
            mRemoves.clear();
        }
        
        if(mLast.getY() < 300f)
        {
            spawn();
        }
        
        for(Block b : mActive)
        {
            b.act(dt);
        }
    }
    
    private Block spawnBlock(int[] types, float x, float y)
    {
        int type = types[MathUtils.random(0, types.length - 1)];
        Block b = mPool.get(type).get(mIndices[type]);
        mIndices[type]++;
        mIndices[type] %= mPool.get(type).size();
        b.spawn(x, y);
        if(!mActive.contains(b))
        {
            mActive.add(b);
        }
        return b;
    }
    
    private Block spawnLeft(Block prev)
    {
        int[] i = prev.getLeft();
        float x = prev.getLeftX();
        float y = prev.getY();
        
        int ix = Math.round(x) / 144;
        int iy = Math.round(y) / 144;
        System.out.println("LEFT: " + ix + ", " + iy);
        
        return spawnBlock(i, x, y);
    }
    
    private Block spawnRight(Block prev)
    {
        int[] i = prev.getRight();
        float x = prev.getRightX();
        float y = prev.getY();
        
        int ix = Math.round(x) / 144;
        int iy = Math.round(y) / 144;
        System.out.println("RIGHT: " + ix + ", " + iy);
        
        return spawnBlock(i, x, y);
    }
    
    private Block spawnFront(Block prev)
    {
        int[] i = prev.getFront();
        float x = prev.getX();
        float y = prev.getFrontY();
        
        int ix = Math.round(x) / 144;
        int iy = Math.round(y) / 144;
        System.out.println("FRONT: " + ix + ", " + iy);
        
        mLast = spawnBlock(i, x, y);
        return mLast;
    }
    
    private void spawn()
    {
        ArrayList<Block> newlyAdded = new ArrayList<Block>();
        for(Block b : mOpenRoads)
        {
            if(b.getLeft() != null)
            {
                newlyAdded.add(spawnLeft(b));
            }
            if(b.getRight() != null)
            {
                newlyAdded.add(spawnRight(b));
            }
            if(b.getFront() != null)
            {
                newlyAdded.add(spawnFront(b));
            }
        }
        mOpenRoads.clear();
        mOpenRoads.addAll(newlyAdded);
    }
    
    public List<Block> getActiveBuildings()
    {
        return mActive;
    }

    private ArrayList<Block> mRemoves = new ArrayList<Block>();
    
    void remove(Block building)
    {
        mRemoves.add(building);
    }

    
}
